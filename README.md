## PFB Impulse Response

### Usage

In the `matlab` subdirectory:

```
>> run_pfb_impulse_response
```

### Installation

The Python components of this project are Python 3.6+.

This comes bundled with a little script for plotting DADA files. With [poetry](https://github.com/sdispater/poetry) installed:

```
poetry update
poetry run python plot_dada_file.py /path/to/dada.dump
```
