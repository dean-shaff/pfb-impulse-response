function res = pfb_impulse_response(...
    n_chan,...
    os_factor,...
    n_bins,...
    fir_filter_path,...
    analysis_handler,...
    offsets,...
    widths,...
    n_pol_,...
    header_path_,...
    dtype_,...
    output_dir_,...
    output_file_name_...
)
  % Generate single channel test vector, and channelize it
  %
  % Args:
  %   n_chan (numeric): Number of PFB channels.
  %   os_factor ({struct}): oversampling factor struct, of form ``struct('nu', 1, 'de', 1)``
  %   n_bins (numeric): Number of single channel samples to generate
  %   fir_filter_path (string): Path to PFB FIR coefficients
  %   analysis_handler (handle): Handle to PFB channelizer function
  %   offsets ([numeric]): List of offsets passed to time_domain_impulse
  %   widths ([numeric]): List of widths passed to time_domain_impulse
  %   n_pol_ (numeric): (Optional) Number of polarizations. Defaults to 1
  %   header_path_ (string): (Optional) Path to header JSON file. Defaults to './default_header.json'
  %   dtype_ (string): (Optional) Data type of output data. Defaults to 'single'
  %   output_dir_ (string): (Optional) Output directory.
  %   output_file_name_ (string): (Optional) Base output file name
  % Returns:
  %   cell: contains two strings with the paths to the impulse, and the channelized impulse

  n_pol = 1;
  if exist('n_pol_', 'var')
    n_pol = n_pol_;
  end

  header_path = 'default_header.json';
  if exist('header_path_', 'var')
    header_path = header_path_;
  end

  dtype = 'single';
  if exist('dtype_', 'var')
    dtype = dtype_;
  end

  output_dir = './';
  if exist('output_dir_', 'var')
    output_dir = output_dir_;
  end

  output_file_name = 'impulse';
  if exist('output_file_name_', 'var')
    output_file_name = strrep(output_file_name_, '.dump', '');
  end

  filter_struct = load(fir_filter_path);
  fir_filter_coeff = filter_struct.filter_new;
  % fir_filter_coeff = read_fir_filter_coeff(fir_filter_path);
  ntaps = length(fir_filter_coeff);

  % load the default header into a struct, and then a containers.Map object.
  json_str = fileread(header_path);
  default_header = struct2map(jsondecode(json_str));

  os_factor_str = sprintf('%d/%d', os_factor.nu, os_factor.de);

  % generate time domain impulse
  impulse_data = time_domain_impulse(n_bins, offsets, widths, dtype);
  input_data = complex(zeros(n_pol, 1, n_bins, dtype));
  for i_pol=1:n_pol
    input_data(i_pol, 1, :) = impulse_data(:);
  end

  input_header = default_header;

  input_data_file_name = sprintf('%s.dump', output_file_name);

  input_data_file_path = fullfile(output_dir, input_data_file_name);
  save_file(input_data_file_path, @write_dada_file, {input_data, input_header});

  channelized = analysis_handler(...
    input_data, fir_filter_coeff, n_chan, os_factor);

  channelized_header = default_header;
  input_tsamp = str2num(channelized_header('TSAMP'));
  channelized_header('TSAMP') = num2str(n_chan*normalize(os_factor, input_tsamp));
  channelized_header('OS_FACTOR') = os_factor_str;
  channelized_header('PFB_DC_CHAN') = '1';
  channelized_header('NCHAN_PFB_0') = num2str(n_chan);

  % save channelized data
  channelized_data_file_name = sprintf('channelized.%s', input_data_file_name);
  channelized_data_file_path = fullfile(output_dir, channelized_data_file_name);
  add_fir_filter_to_header(channelized_header, fir_filter_coeff, os_factor);
  save_file(channelized_data_file_path, @write_dada_file, {channelized, channelized_header})

  res = {input_data_file_path, channelized_data_file_path};

end
